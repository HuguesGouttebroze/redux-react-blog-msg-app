import pogues from './assets/pogues.jpg'
import User from './components/User'
import PostForm from './components/PostForm'

function App() {

  return (
    <>
      <header>
        <h1>Music Passion Sharring</h1>
        <img 
          src={pogues} 
          alt='legend music band' 
          className='img-band'
        /> 
      </header>
      <PostForm />
      <div className='content'>
        <div className="post-container">contenu</div>
        <User />
      </div>
    </>
  )
}

export default App
