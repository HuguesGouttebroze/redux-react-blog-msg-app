// eslint-disable-next-line no-unused-vars
import React from "react";

const PostForm = () => {
  return (
    <div className="form-container">
      <form>
        <input type="text" placeholder="Titre du poste" />
        <textarea placeholder="Et toi, t'en penses quoi?"></textarea>
        <input type="submit" value="Envoyer" />
      </form>
    </div>
  );
};

export default PostForm;
